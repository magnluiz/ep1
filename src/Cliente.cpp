#include "Cliente.hpp"
#include <string>
#include <string.h>
#include <iostream>
using namespace std;

Cliente::Cliente(string cpf, string cep, string telefone, string nome, string email, string sexo,bool socio){
    this->set_cpf(cpf);
    this->set_cep(cep);
    this->set_telefone(telefone);
    this->set_nome(nome);
    this->set_email(email);
    this->set_sexo(sexo);
    this->set_socio(socio);
    
}
Cliente::Cliente(){
    cpf = "n/a";
    cep = "n/a";
    telefone = "n/a";
    nome = "n/a";
    email = "n/a";
    sexo = "n/a";
    socio = false;
}

void Cliente::set_compras(vector<string>&compras){
    this->compras = compras;
}
void Cliente::set_cpf(string cpf){
    this->cpf = cpf;
}
void Cliente::set_cep(string cep){
    this->cep = cep;
}
void Cliente::set_telefone(string telefone){
    this->telefone = telefone;
}
void Cliente::set_nome(string nome){
    this->nome = nome;
}
void Cliente::set_email(string email){
    this->email = email;
}
void Cliente::set_sexo(string sexo){
    this->sexo = sexo;
}
void Cliente::set_socio(bool socio){
    this->socio = socio;
}
string Cliente::get_cpf(){
    return this->cpf;
}
string Cliente::get_cep(){
    return this->cep;
}
string Cliente::get_telefone(){
    return this->telefone;
}
string Cliente::get_nome(){
    return this->nome;
}
string Cliente::get_email(){
    return this->email;
}
string Cliente::get_sexo(){
    return this->sexo;
}
bool Cliente::get_socio(){
    return this->socio;
}
vector<string> Cliente::get_compras(){
    return this->compras;
}
