#include "Produto.hpp"
#include <string>
#include <iostream>
using namespace std;

Produto::Produto(int qtd , string codigo, double custo, string categoria,string titulo)
{
    this->set_qtd(qtd);
    this->set_codigo(codigo);
    this->set_custo(custo);
    this->set_categoria(categoria);
    this->set_titulo(titulo);
    
}
Produto::Produto(){
    
}
void Produto::set_qtd(int qtd)
{
    this->qtd = qtd;
}
void Produto::set_codigo(string codigo)
{
    this->codigo = codigo;
}
void Produto::set_custo(double custo)
{
    this->custo = custo;
}
void Produto::set_categoria(string categoria)
{
    this->categoria = categoria;
}
void Produto::set_titulo(string titulo)
{
    this->titulo = titulo;
}

int Produto::get_qtd(){
    return this->qtd;
}
string Produto::get_codigo(){
    return this->codigo;
}
double Produto::get_custo(){
    return this->custo;
}
string Produto::get_categoria(){
    return this->categoria;
}
string Produto::get_titulo(){
    return this->titulo;
}

