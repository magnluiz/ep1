#include <string>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <fstream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>

#include "Cliente.hpp"
#include "Produto.hpp"
#include "Cadastro.hpp"
#include "Arquivos.hpp"

using namespace std;

void salvar(vector<Cliente*> &clientes){
    ofstream outfile;
    outfile.open("../ep1/doc/Clientes.txt");
    for (int i = 0; i < clientes.size();i++){

        outfile << (*clientes[i]).get_nome();
        outfile << '\n';
        outfile << (*clientes[i]).get_cpf();
        outfile << '\n';
        outfile << (*clientes[i]).get_cep();
        outfile << '\n';
        outfile << (*clientes[i]).get_telefone();
        outfile << '\n';
        outfile << (*clientes[i]).get_email();
        outfile << '\n';
        outfile << (*clientes[i]).get_sexo();
        outfile << '\n';
        outfile << (*clientes[i]).get_socio();
        outfile << endl;
        
        
    }
    outfile.close();
}
void salvar(vector<Produto*> &estoque){
    ofstream outfile;
    outfile.open("../ep1/doc/Produtos.txt");
    for (int i = 0; i < estoque.size();i++){

        outfile << (*estoque[i]).get_qtd();
        outfile << '\n';
        outfile << (*estoque[i]).get_codigo();
        outfile << '\n';
        outfile << (*estoque[i]).get_custo();
        outfile << '\n';
        outfile << (*estoque[i]).get_categoria();
        outfile << '\n';
        outfile << (*estoque[i]).get_titulo();
        outfile << endl;
        
    }
    outfile.close();

}
int c_linhas(string arquivo){
    ifstream cinfile;
    cinfile.open(arquivo);
    int i = 0;
    string linhaAtual;
    while (cinfile >> linhaAtual)
    {
        i+=1;
    }
    cinfile.close();
    return i;
    
}

void carregar(vector<Cliente*> &clientes){
    ifstream infile;
    infile.open("../ep1/doc/Clientes.txt");
    for (int i = 0; i <= c_linhas("../ep1/doc/Clientes.txt"); i++){
        string nome,cpf,cep,telefone,email,sexo,socio;
        bool vsocio;
        getline (infile,nome);
        getline (infile,cpf);
        getline (infile,cep);
        getline (infile,telefone);
        getline (infile,email);
        getline (infile,sexo);
        getline (infile,socio);
        if (socio == "1"){
            vsocio = true;
        }
        else {
            vsocio = false;
        }
        
        
        clientes.push_back(new Cliente(cpf,cep,telefone,nome,email,sexo,vsocio));
        
        
    }
    

    infile.close();
    
    
}
void carregar(vector<Produto*>&estoque){
    ifstream infile;
    infile.open("../ep1/doc/Produtos.txt");
    for (int i = 0;i <= c_linhas("../ep1/doc/Produtos.txt");i++){
        string qtd;
        string codigo; 
        string custo;
        string categoria;
        string titulo;

        getline(infile,qtd);
        getline(infile,codigo);
        getline(infile,custo);
        getline(infile,categoria);
        getline(infile,titulo);
        
        
        
        int vqtd = atoi(qtd.c_str());
        double vcusto = atof(custo.c_str());
        estoque.push_back(new Produto(vqtd,codigo,vcusto,categoria,titulo));
        


    }
    infile.close();
}











