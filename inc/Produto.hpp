#ifndef Produto_hpp
#define Produto_hpp
#include <string>
#include <iostream>
using namespace std;

class Produto
{
private:
    int qtd;// quantidade
    string codigo;
    double custo; // preço
    string categoria;
    string titulo; //nome
public:
    Produto(int qtd , string codigo, double custo, string categoria,string titulo);
    Produto();

    void set_qtd(int qtd);
    void set_codigo(string codigo);
    void set_custo(double custo);
    void set_categoria(string categoria);
    void set_titulo(string titulo);

    int get_qtd();
    string get_codigo();
    double get_custo();
    string get_categoria();
    string get_titulo();
    

};




#endif