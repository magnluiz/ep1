#ifndef Cliente_hpp
#define Cliente_hpp

#include <string>
#include <vector>
#include <iostream>
#include <vector>

using namespace std;

class Cliente
{
private:
    string cpf; // 000000000-00
    string cep; // 00000-000
    string telefone; // (00)00000000 tudo junto sem parênteses
    string nome;
    string email; 
    string sexo; // 
    bool socio;
    vector<string> compras;
public:
    
    Cliente(); // construtor polimórfico no caso de um cliente que não informou nada
    
    Cliente(string cpf, string cep, string telefone, string nome, string email, string sexo,bool socio);
    void set_cpf(string cpf);
    void set_cep(string cep);
    void set_telefone(string telefone);
    void set_nome(string nome);
    void set_email(string email);
    void set_sexo(string sexo);
    void set_socio(bool socio);
    void set_compras(vector<string>&compras);
    string get_cpf();
    string get_cep();
    string get_telefone();
    string get_nome();
    string get_email();
    string get_sexo();
    bool get_socio();
    vector <string> get_compras();
    
};
#endif