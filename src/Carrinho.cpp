#include <string>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <fstream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

#include "Cliente.hpp"
#include "Produto.hpp"
#include "Cadastro.hpp"
#include "Arquivos.hpp"
#include "Carrinho.hpp"

using namespace std;

Carrinho::Carrinho(vector<Produto*> &estoque,vector<Cliente*> &clientes,string nome){ //é chamado com o estoque , os cliente e o nome do cliente.
    this->estoque = estoque;
    this->clientes = clientes;
    this->nome = nome;
}
void Carrinho::comprar(vector<Produto*> &carrinho){ // e chamada com carrinho e salva clientes
    sort(carrinho.begin(),carrinho.end());
    carrinho.erase( unique( carrinho.begin(), carrinho.end() ), carrinho.end());
    for (int i = 0;i<carrinho.size();i++){
        vector <string> categoria;
        categoria[i] = carrinho[i]->get_categoria();
        removerqtd(estoque,carrinho[i]->get_titulo());
        for (int i = 0;i<clientes.size();i++){
            if (clientes[i]->get_nome() == nome){
                clientes[i]->set_compras(categoria);  // em produto categoria e uma string em cliente categoeria e um vetor de strings
                return;
            }
        
        }
        salvar(clientes);
        
    }



    }
    

void Carrinho::adicionar(vector<Produto*> &carrinho,string nome){ // e chamada com carrinho
    for (int i = 0;estoque.size();i++){
        if (estoque[i]->get_titulo() == nome){
            carrinho.push_back(new Produto(estoque[i]->get_qtd(),estoque[i]->get_codigo(),estoque[i]->get_custo(),estoque[i]->get_categoria(),estoque[i]->get_titulo()));
            return;
        }   
    }
    cout << "Nao encontrado";
}
void Carrinho::remover(vector<Produto*> &carrinho,string nome){ // e chamada com carrinho
    for (int i =0;carrinho.size();i++){
        if (carrinho[i]->get_titulo() == nome){
            carrinho.erase(carrinho.begin() + i);
            return;
        }
    }
    cout << "Nao encontrado";
}
void Carrinho::mostrar_custo_total(vector<Produto*> &carrinho){ // e chamada com carrinho
    double custo = 0;

    for (int i = 0;carrinho.size();i++){
        cout << carrinho[i]->get_titulo() << "        " << "preço:" << carrinho[i]->get_custo() << "R$";
        custo += carrinho[i]->get_custo();

    }
    cout << "O total é: " << custo;
}
void Carrinho::removerqtd(vector<Produto*> &estoque,string nome){ // e chamada com estoque e salva estoque
    for (int i = 0;estoque.size();i++){
        if (estoque[i]->get_titulo() == nome){
            estoque[i]->set_qtd(estoque[i]->get_qtd() - 1);
        }

    }
    salvar(estoque);
}