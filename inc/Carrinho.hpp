#ifndef Carrinho_hpp
#define Carrinho_hpp

#include <string>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <fstream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>

#include "Cliente.hpp"
#include "Produto.hpp"
#include "Cadastro.hpp"
#include "Arquivos.hpp"

using namespace std;

class Carrinho : public Produto, public Cliente
{
private:
    vector<Produto*> estoque;
    vector<Produto*> carrinho;
    vector<Cliente*> clientes;
    string nome;
    Carrinho(vector<Produto*> &estoque,vector<Cliente*> &clientes,string nome);
    void comprar(vector<Produto*> &carrinho);
    void adicionar(vector<Produto*> &carrinho, string nome);
    void remover(vector<Produto*> &carrinho,string nome);
    void mostrar_custo_total(vector<Produto*> &carrinho);
    void removerqtd(vector<Produto*> &estoque,string nome);
    
    
};




#endif